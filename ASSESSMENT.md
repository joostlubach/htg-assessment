# HumanTalentGroup Assessment

## Outline & background

HumanTalentGroup is currently working on a new product line called _BeWizr_, which incorporates components from the current _BeWizr_ product as well as the current _GroundControl_ product. The latter is a realtime distributed system focused on creating and managing microcommunities.

As a real-time system, the communication between our clients and server applications needs to be _bidirectional_. In this assessment, we ask you to write a small chat app consists of two components:

1. A **client application** which would run in a browser
1. An **service application** which would run on the server (NodeJS)

The app has three business requirements:

1. If I open the app, I am placed in a single chatroom with everybody else. Whatever I type and send will be seen by all other users of the application (connected to the same server that is). 
2. When I open the app, I first have to enter my name. My name is shown every time a message from me is displayed in the chat room.
3. Technically: the _chat service_ should exchange all messages. Everything the API should to is pass messages from the client to the service and vice versa. See below for more information about this.

This assessment should not take more than about 2-4 hours. To that end, let's give some negative requirements :), things that we do _not_ need to see in the application.

1. The application does not have to persist chats. I.e., don't spend time on choosing a database. The server is not supposed to remember anything, just serve as a mailbox.
2. Don't worry about styling. Feel free to use anything like Bootstrap or just plain HTML. It should work, it doesn't have to look pretty.

Some other notes:

1. The test needs to be completed in TypeScript. Plain JavaScript is not allowed.
2. Fork this repository to hand in your test.

### Part 1 - choosing technologies

Research technologies that provides realtime communication between a browser application and an application running in NodeJS. List a few, name their pros and cons, and finally choose one you would use.

> Please note: we will not judge the choice you made, there is no "correct" technology. However, we will look at your decision process.

### Part 2 - writing the components

**2a.** Write the two components that make up the app. You can use this repository as a template. Feel free to modify the repository however. Perhaps you want to create some common code, perhaps you feel that some things require a different set up. All good.

**2b.** Write unit tests. In our philosophy, you don't need to achieve 100% coverage. Determine for yourself which parts of the code require testing, and write tests for those. You can use _jest_ for testing.

> Notes: we use `vite` for bundling browser resources, and `yarn` for package management. Feel free to use webpack or anything you're familiar with. We would like that most of your time goes into the application, and as little as possible to setting up the environment. To run the client in vite, simply type: `yarn vite` from the `/client` directory.

### Part 3 - writing documentation

Don't worry about documenting everything. All we need is a `HANDIN.md` that outlines the choices you made, and an instruction on how to run the application. 