# API Component

This component is responsible for accepting messages from a browser, and passing them on to the `service` component, and vice versa.

# Running &amp; testing

- Run the app with `yarn dev`.
- Test the app with `yarn test`.