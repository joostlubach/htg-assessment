module.exports = {
  preset: 'ts-jest',
  transform: {
    '^.+\\.tsx?$': [
      'ts-jest',
      {
        isolatedModules: true
      }
    ]
  },
  testMatch: ['<rootDir>/src/**/*.test.ts'],
  testEnvironment: 'node'
}