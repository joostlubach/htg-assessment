# Client application

This is the application that runs in the browser.

# Running &amp; testing

- Run the app with `yarn dev`.
- Test the app with `yarn test`.